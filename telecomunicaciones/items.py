# -*- coding: utf-8 -*-

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, Compose, Join, MapCompose, TakeFirst
from w3lib.html import remove_tags
import time

class TelecomunicacionesItem(scrapy.Item):
    id_producto = scrapy.Field()
    nombre  = scrapy.Field()
    categoria  = scrapy.Field()
    peso = scrapy.Field()
    marca = scrapy.Field()
    url_imagen = scrapy.Field()
    hora_extraccion = scrapy.Field()
    fecha_extraccion = scrapy.Field()
    fuente = scrapy.Field()
    url  = scrapy.Field()
    url_imagen = scrapy.Field()
    disponibilidad = scrapy.Field()
    #Datos no estructurados
    detalles_precios = scrapy.Field()
    detalles_generales = scrapy.Field()
    #VTR
    contenido_pack = scrapy.Field()
    descuento_primer_mes = scrapy.Field()
    otros_detalles = scrapy.Field()

    #datos de planes con moviles
    nombre_equipo = scrapy.Field()
    id_plan = scrapy.Field()
    nombre_plan = scrapy.Field()
    valor_inicial = scrapy.Field()
    cargo_fijo = scrapy.Field()
    minutos = scrapy.Field() 
    navegacion = scrapy.Field() 
    portabilidad  = scrapy.Field()

    #datos de planes  sin movil
    cargofijo_equipoporta = scrapy.Field()
    cargofijo_equiponuevo = scrapy.Field()

    #SUBTEL
    nombre_empresa =  scrapy.Field()
    nombre_plan =  scrapy.Field() 
    valor_plan =  scrapy.Field()
    minutos_incluidos =  scrapy.Field() 
    velocidad_bajada = scrapy.Field()
    cantidad_canales = scrapy.Field()
    region = scrapy.Field()
    comuna = scrapy.Field()
    servicio = scrapy.Field()


class SubtelLoader(ItemLoader):
    default_item_class = TelecomunicacionesItem

    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','').strip()
    def process_price(value):
        if value:
            return value.replace('$','').replace('.','')

    
    nombre_empresa_in = Compose(Join(),strip_dashes)
    nombre_plan_in = Compose(Join(),strip_dashes) 
    valor_plan_in = Compose(Join(),strip_dashes,process_price)
    minutos_incluidos_in = Compose(Join(),strip_dashes)
    velocidad_bajada_in = Compose(Join(),strip_dashes)
    cantidad_canales_in = Compose(Join(),strip_dashes)
    region_in = Compose(Join(),strip_dashes)
    comuna_in = Compose(Join(),strip_dashes)
    servicio_in = Compose(Join(),strip_dashes)


    region_out = TakeFirst()
    comuna_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    nombre_empresa_out =TakeFirst()
    nombre_plan_out =TakeFirst() 
    valor_plan_out =TakeFirst()
    minutos_incluidos_out =TakeFirst()
    velocidad_bajada_out = TakeFirst()
    cantidad_canales_out = TakeFirst()
    region_out = TakeFirst()
    comuna_out = TakeFirst()
    servicio_out = TakeFirst()


class MovilesLoader(ItemLoader):
    default_item_class = TelecomunicacionesItem

    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','').strip()
    
    clear_entrada = Compose(Join(),strip_dashes)
    nombre_equipo_in = clear_entrada
    id_plan_in = clear_entrada
    nombre_plan_in = clear_entrada
    valor_inicial_in = clear_entrada
    cargo_fijo_in = clear_entrada
    minutos_in = clear_entrada 
    navegacion_in = clear_entrada 
    cargofijo_equipoporta_in = clear_entrada
    cargofijo_equiponuevo_in = clear_entrada
    portabilidad_in = clear_entrada




    clear_salida = TakeFirst()
    nombre_equipo_out = clear_salida
    id_plan_out = clear_salida
    nombre_plan_out =     clear_salida
    valor_inicial_out =   clear_salida
    cargo_fijo_out =  clear_salida
    minutos_out =     clear_salida 
    navegacion_out =  clear_salida 
    portabilidad_out =   clear_salida
    url_imagen_out = clear_salida
    hora_extraccion_out = clear_salida
    fecha_extraccion_out = clear_salida
    fuente_out = clear_salida
    url_out  = clear_salida
    cargofijo_equipoporta_out= clear_salida
    cargofijo_equiponuevo_out= clear_salida


class TelecomunicacionesLoader(ItemLoader):
    default_item_class = TelecomunicacionesItem

    def price_processor(v):
        if v:
            return v.replace('.', '').replace('$','').replace('*','')
    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','')
    def strip_others(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','')


    categoria_in  = Compose(Join(),strip_dashes)
    peso_in = Compose(Join(),strip_dashes)
    #Datos no estructurados
    detalles_precios_in = Compose(Join(),strip_dashes)
    detalles_tecnicos_in = Compose(Join(),strip_dashes)
    detalles_generales_in = Compose(Join(),strip_dashes)
    otros_detalles_in = Compose(Join(),strip_dashes)
    contenido_pack_in = Compose(Join(),strip_dashes)
    descuento_primer_mes_in = Compose(Join(),strip_dashes)

    nombre_in = Compose(Join(),strip_dashes)
    #id_producto_in = Compose(Join(),strip_dashes)

    #Valores de salida
    url_imagen_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    #nombre_out = TakeFirst()
    id_producto_out = TakeFirst()


class EntelLoader(ItemLoader):
    default_item_class = TelecomunicacionesItem

    def price_processor(v):
        if v:
            return v.replace('.', '').replace('$','').replace('*','')
    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','')
    def strip_others(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','')


    categoria_in  = Compose(Join(),strip_dashes)
    peso_in = Compose(Join(),strip_dashes)
    #Datos no estructurados
    detalles_precios_in = Compose(Join(),strip_dashes)
    detalles_tecnicos_in = Compose(Join(),strip_dashes)
    detalles_generales_in = Compose(Join(),strip_dashes)
    otros_detalles_in = Compose(Join(),strip_dashes)
    contenido_pack_in = Compose(Join(),strip_dashes)
    descuento_primer_mes_in = Compose(Join(),strip_dashes)

    nombre_in = Compose(Join(),strip_dashes)
    #id_producto_in = Compose(Join(),strip_dashes)

    #Valores de salida
    url_imagen_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    categoria_out = TakeFirst()
    #nombre_out = TakeFirst()
    id_producto_out = TakeFirst()

class EntelprepagoLoader(ItemLoader):
    default_item_class = TelecomunicacionesItem

    def price_processor(v):
        if v:
            return v.replace('.', '').replace('$','').replace('*','')
    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace('\xa0','')
    def strip_others(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','')


    categoria_in  = Compose(Join(),strip_dashes)
    peso_in = Compose(Join(),strip_dashes)
    #Datos no estructurados
    detalles_precios_in = Compose(Join(),strip_dashes)
    detalles_tecnicos_in = Compose(Join(),strip_dashes)
    detalles_generales_in = Compose(Join(),strip_dashes)
    otros_detalles_in = Compose(Join(),strip_dashes)
    contenido_pack_in = Compose(Join(),strip_dashes)
    descuento_primer_mes_in = Compose(Join(),strip_dashes)

    nombre_in = Compose(Join(),strip_dashes)
    #id_producto_in = Compose(Join(),strip_dashes)

    #Valores de salida
    url_imagen_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    categoria_out = TakeFirst()
    nombre_out = TakeFirst()
    id_producto_out = TakeFirst()

