# -*- coding: utf-8 -*-

from telecomunicaciones import items
from telecomunicaciones import settings
from telecomunicaciones.items import *
import csv


class TelecomunicacionesPipeline(object):
	def __init__(self):
		self.file_name = {}
		self.conmovil = {'nombre_equipo','id_plan','nombre_plan','valor_inicial','cargo_fijo','minutos','navegacion','portabilidad','hora_extraccion','fecha_extraccion','fuente','url'}
		self.vtr_hogar = {	'id_producto','nombre','contenido_pack','detalles_generales','detalles_precios','descuento_primer_mes','otros_detalles','url','fuente','fecha_extraccion','hora_extraccion'}
		self.movistar = {'nombre','detalles_precios','categoria','detalles_generales','url','fuente','fecha_extraccion','hora_extraccion'}
		self.subtel_fija_cl = {"nombre_empresa","nombre_plan","valor_plan","minutos_incluidos","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"}
		self.subtel_tv_cl = {"nombre_empresa","nombre_plan","valor_plan","cantidad_canales","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"}
		self.subtel_internet_cl = {"nombre_empresa","nombre_plan","valor_plan","velocidad_bajada","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"}
		self.subtel_tv_fija_cl = {"nombre_empresa","nombre_plan","valor_plan","cantidad_canales","minutos_incluidos","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"}
		self.subtel_tv_internet_cl = {"nombre_empresa","nombre_plan","valor_plan","cantidad_canales","velocidad_bajada","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"}
		self.subtel_fija_internet_cl = {"nombre_empresa","nombre_plan","valor_plan","minutos_incluidos","velocidad_bajada","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"}
		self.subtel_trio_cl = {"nombre_empresa","nombre_plan","valor_plan","minutos_incluidos","velocidad_bajada","cantidad_canales","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"}

	def open_spider(self, spider):
		self.file_name=csv.writer(open('output_'+spider.name+'.csv','w'))
		
		if spider.name.find('vtr_conmovil')!= -1:
			self.file_name.writerow(['nombre_equipo','id_plan','nombre_plan','valor_inicial','cargo_fijo','minutos','navegacion','portabilidad','hora_extraccion','fecha_extraccion','fuente','url'])
		if spider.name.find('vtr_sinmovil')!= -1:
			self.file_name.writerow(['id_plan','nombre_plan','cargofijo_equiponuevo','cargofijo_equipoporta','navegacion','minutos','url','fuente','fecha_extraccion','hora_extraccion'])
		if spider.name.find('vtr_hogar')!= -1:
			self.file_name.writerow(['id_producto','nombre','contenido_pack','detalles_generales','detalles_precios','descuento_primer_mes','otros_detalles','url','fuente','fecha_extraccion','hora_extraccion'])
		if spider.name.find('movistar')!= -1 or spider.name.find('entel')!= -1 or spider.name.find('claro')!= -1:
			self.file_name.writerow(['nombre','detalles_precios','categoria','detalles_generales','url','fuente','fecha_extraccion','hora_extraccion'])
		if spider.name.find('subtel_fija_cl')!= -1 or spider.name.find('subtel_movil_cl')!= -1:
			self.file_name.writerow(["nombre_empresa","nombre_plan","valor_plan","minutos_incluidos","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"])
		if spider.name.find('subtel_tv_cl')!= -1:
			self.file_name.writerow(["nombre_empresa","nombre_plan","valor_plan","cantidad_canales","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"])
		if spider.name.find('subtel_internet_cl')!= -1:
			self.file_name.writerow(["nombre_empresa","nombre_plan","valor_plan","velocidad_bajada","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"])
		if spider.name.find('subtel_tv_fija_cl')!= -1:
			self.file_name.writerow(["nombre_empresa","nombre_plan","valor_plan","cantidad_canales","minutos_incluidos","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"])
		if spider.name.find('subtel_tv_internet_cl')!= -1:
			self.file_name.writerow(["nombre_empresa","nombre_plan","valor_plan","cantidad_canales","velocidad_bajada","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"])
		if spider.name.find('subtel_fija_internet_cl')!= -1:
			self.file_name.writerow(["nombre_empresa","nombre_plan","valor_plan","minutos_incluidos","velocidad_bajada","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"])
		if spider.name.find('subtel_trio_cl')!= -1:
			self.file_name.writerow(["nombre_empresa","nombre_plan","valor_plan","minutos_incluidos","velocidad_bajada","cantidad_canales","servicio","region","comuna","hora_extraccion","fecha_extraccion","fuente"])


 


	def process_item(self, item, spider):
		if spider.name.find('vtr_conmovil')!= -1:
			self.file_name.writerow([
							item['nombre_equipo']
							,item['id_plan']
							,item['nombre_plan']
							,item['valor_inicial']
							,item['cargo_fijo']
							,item['minutos']
							,item['navegacion']
							,item['portabilidad']
							,item['hora_extraccion']
							,item['fecha_extraccion']
							,item['fuente']
							,item['url']
					])
			return item
		if spider.name.find('vtr_sinmovil')!= -1:
			self.file_name.writerow([
							item['id_plan'],
							item['nombre_plan'],
							item['cargofijo_equiponuevo'],
							item['cargofijo_equipoporta'],
							item['navegacion'],
							item['minutos'],
							item['url'],
							item['fuente'],
							item['fecha_extraccion'],
							item['hora_extraccion'],
					])
			return item
		if spider.name.find('vtr_hogar')!= -1:
			for key in self.vtr_hogar:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item['id_producto'],
							item['nombre'],
							item['contenido_pack'],
							item['detalles_generales'],
							item['detalles_precios'],
							item['descuento_primer_mes'],
							item['otros_detalles'],
							item['url'],
							item['fuente'],
							item['fecha_extraccion'],
							item['hora_extraccion'],
					])
			return item
		if spider.name.find('movistar')!= -1 or spider.name.find('entel')!= -1 or spider.name.find('claro')!= -1:
			for key in self.movistar:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item['nombre'],
							item['detalles_precios'],
							item['categoria'],
							item['detalles_generales'],
							item['url'],
							item['fuente'],
							item['fecha_extraccion'],
							item['hora_extraccion'],
					])
			return item

		#TELEFONIA MOVIL Y FIJA
		if spider.name.find('subtel_fija_cl')!= -1 or spider.name.find('subtel_movil_cl')!= -1:
			for key in self.subtel_fija_cl:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item["nombre_empresa"],
							item["nombre_plan"],
							item["valor_plan"],
							item["minutos_incluidos"],
							item["servicio"],
							item["region"],
							item["comuna"],
							item["hora_extraccion"],
							item["fecha_extraccion"],
							item["fuente"]
					])
			return item
		#TELEVISION
		if spider.name.find('subtel_tv_cl')!=-1:
			for key in self.subtel_tv_cl:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item["nombre_empresa"],
							item["nombre_plan"],
							item["valor_plan"],
							item["cantidad_canales"],
							item["servicio"],
							item["region"],
							item["comuna"],
							item["hora_extraccion"],
							item["fecha_extraccion"],
							item["fuente"]
					])
			return item
		#INTERNET
		if spider.name.find('subtel_internet_cl')!=-1:
			for key in self.subtel_internet_cl:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item["nombre_empresa"],
							item["nombre_plan"],
							item["valor_plan"],
							item["velocidad_bajada"],
							item["servicio"],
							item["region"],
							item["comuna"],
							item["hora_extraccion"],
							item["fecha_extraccion"],
							item["fuente"]
					])
			return item

		#TV - FIJA
		if spider.name.find('subtel_tv_fija_cl')!=-1:
			for key in self.subtel_tv_fija_cl:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item["nombre_empresa"],
							item["nombre_plan"],
							item["valor_plan"],
							item["cantidad_canales"],
							item["minutos_incluidos"],
							item["servicio"],
							item["region"],
							item["comuna"],
							item["hora_extraccion"],
							item["fecha_extraccion"],
							item["fuente"]
					])
			return item

		#TV - INTERNET
		if spider.name.find('subtel_tv_internet_cl')!=-1:
			for key in self.subtel_tv_fija_cl:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item["nombre_empresa"],
							item["nombre_plan"],
							item["valor_plan"],
							item["cantidad_canales"],
							item["velocidad_bajada"],
							item["servicio"],
							item["region"],
							item["comuna"],
							item["hora_extraccion"],
							item["fecha_extraccion"],
							item["fuente"]
					])
			return item

		#FIJA - INTERNET
		if spider.name.find('subtel_fija_internet_cl')!=-1:
			for key in self.subtel_tv_fija_cl:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item["nombre_empresa"],
							item["nombre_plan"],
							item["valor_plan"],
							item["minutos_incluidos"],
							item["velocidad_bajada"],
							item["servicio"],
							item["region"],
							item["comuna"],
							item["hora_extraccion"],
							item["fecha_extraccion"],
							item["fuente"]
					])
			return item

		#TRIO
		if spider.name.find('subtel_trio_cl')!=-1:
			for key in self.subtel_trio_cl:
				item.setdefault(key,'NA')
			self.file_name.writerow([
							item["nombre_empresa"],
							item["nombre_plan"],
							item["valor_plan"],
							item["minutos_incluidos"],
							item["velocidad_bajada"],
							item["cantidad_canales"],
							item["servicio"],
							item["region"],
							item["comuna"],
							item["hora_extraccion"],
							item["fecha_extraccion"],
							item["fuente"]
					])
			return item
