# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule, Spider
from scrapy.http import Request,FormRequest
from telecomunicaciones.items import TelecomunicacionesLoader
import time

class subtel_cl(CrawlSpider):
	name = 'subtel_cl'
	start_urls = ["http://cpt.subtel.cl/ComPlan/regionComuna.do?method=next&codRegionSelected=2&codComunaSelected=2201&x=36&y=23"]

	
	def parse (self, response):
		yield Request(url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=agregarServicio&codServ=4"
					 ,callback = self.parse_comuna
					 )

	def parse_comuna(self, response):

		#Host:cpt.subtel.cl
		#Upgrade-Insecure-Requests:1
		user_agent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
		
		# - 1
		yield FormRequest( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 	 , callback=self.parse_data
					 	 , headers = {'Host':'cpt.subtel.cl'
					 			 	 ,'User-Agent':user_agent
					 			 	 }
					 	 , dont_filter = True
					 	 , meta = {'numero':'1'}
					 	 , formdata={'filtroTM':'-1'
									,'filtroTotalPagar':'-1'
									}
					 )
		# -2 
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_data
					 , dont_filter = True
					 , meta = {'numero':'2'}
					 )
		# -3
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next&filtroTL=-1&filtroIN=-1&filtroTotalPagar=-1&x=37&y=14"
					 , callback=self.parse_data
					 , dont_filter = True
					 , meta = {'numero':'3'}

					 )
		# -4
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_data
					 
					 , dont_filter = True
					 , meta = {'numero':'4'}
					 )
		# -5
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next&filtroTL=-1&filtroIN=-1&filtroTotalPagar=-1"
					 , callback=self.parse_data
					 
					 , dont_filter = True
					 , meta = {'numero':'5'}
					 )

		#################################FORM DATA##################################################33
		# -6
		yield FormRequest( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_data
					 , cookies = {'Cookie':cookie}
					 , formdata={'filtroTM':'-1'
								,'filtroTotalPagar':'-1'
								 ,'x':'36'
								 ,'y':'23'
									}
					 , dont_filter = True
					 , meta = {'numero':'6'}
					 )
		# -7
		yield FormRequest( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next&filtroTL=-1&filtroIN=-1&filtroTotalPagar=-1&x=37&y=14"
					 , callback=self.parse_data
					 , dont_filter = True
					 , meta = {'numero':'7'}
					 )
		# - 8
		yield FormRequest( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_data
					 , cookies = {'Cookie':cookie}
					 , dont_filter = True
					 , formdata={'filtroTM':'-1'
								,'filtroTotalPagar':'-1'
								,'x':'36'
								 ,'y':'23'
								}
					, meta = {'numero':'8'}

					 )
	    # -9
		yield FormRequest( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_data
					 
					 , dont_filter = True
					 , formdata={'filtroTM':'-1'
								,'filtroTotalPagar':'-1'
								,'x':'55'
								,'y':'37'
								}
					, meta = {'numero':'9'}
					 )

		# -10
		yield FormRequest( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_data
					 
					 , dont_filter = True
					 , formdata={'filtroTM':'-1'
								 ,'filtroTotalPagar':'-1'
					#			 ,'x':'55'
					#			 ,'y':'37'
								}
					, meta = {'numero':'10'}
					 )



	#conclusiones
	#Eliminar x e y

	def parse_data(self, response):

		print('*****************************************')
		print(response.meta['numero'])
		print(response.xpath("//img[@id]/@src").extract())
		print(response.xpath("//*[@class='texto_error']//text()").extract())
		print(response.xpath("//td[@class='txt_tablas_chicas']/span[@class='txt_tablas_chicas']//text()").extract())
		print(response.xpath("//td[@class='txt_tabla_comparador_numero'][1]/text()").extract())
		print('*****************************************')




	