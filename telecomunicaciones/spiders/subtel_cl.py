# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule, Spider
from scrapy.http import Request,FormRequest
from telecomunicaciones.items import SubtelLoader
import time

class subtel_cl(CrawlSpider):
	name = 'subtel_cl'
	#start_urls = ["http://cpt.subtel.cl/ComPlan/regionComuna.do?method=next&codRegionSelected=2&codComunaSelected=2201"]

	
	#1 : TELEFONIA_MOVIL
	#2 : TELEFONIA FIJA
	#3 : INTERNET
	#4 : TELEVISION

	def start_requests (self):
	
		yield Request( url="http://cpt.subtel.cl/ComPlan/regionComuna.do?method=next&codRegionSelected=2&codComunaSelected=2201"
					  , callback = self.parse_telefonia_fija
					   , dont_filter = True
					  )
		yield Request( url="http://cpt.subtel.cl/ComPlan/regionComuna.do?method=next&codRegionSelected=2&codComunaSelected=2201"
					  , callback = self.parse_telefonia_movil
					   , dont_filter = True)



	def parse_telefonia_fija(self, response):
		#FIJA
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=agregarServicio&codServ=2"
					 , callback = self.parse_dummy_telefonia
					 , meta = {'funcion':'parse_telefonia'}
					 , dont_filter = True
					 )

	def parse_telefonia_movil (self,response):
		#MOVIL
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=agregarServicio&codServ=1"
					 , callback = self.parse_dummy_telefonia
					 , meta = {'funcion':'parse_telefonia'}
					 , dont_filter = True

					 )

		'''
		#INTERNET
		yield Request(url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=agregarServicio&codServ=1"
					 ,callback = self.parse_dummy
					 ,meta = {'funcion':'parse_telefonia'}
					 )
		#TELEVISION
		yield Request(url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=agregarServicio&codServ=1"
					 ,callback = self.parse_comuna
					 )
		'''
	def parse_dummy_telefonia (self, response):
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_telefonia
					 , dont_filter = True
					 )

	def parse_telefonia(self,response):
		print(response.xpath("//table[@class='tablesorter']//tr").extract())
		for row in response.xpath("//tr"):
			loader = SubtelLoader(response=response, selector = row)
			loader.add_xpath(  "nombre_empresa"			, "./td[2]//text()")
			loader.add_xpath(  "nombre_plan"			, "./td[3]//text()")
			loader.add_xpath(  "valor_plan"				, "./td[4]//text()")
			loader.add_xpath(  "minutos_incluidos"		, "./td[5]//text()")
			#			print(loader.load_item())
