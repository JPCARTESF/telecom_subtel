# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule, Spider
from scrapy.http import Request,FormRequest
from telecomunicaciones.items import SubtelLoader
import time

class subtel_tv_cl(CrawlSpider):
	name = 'subtel_tv_cl'
	comunas = {}
	#XV REGION
	comunas['15101'] = ['15','Arica y Parinacota','Arica']
	comunas['15102'] = ['15','Arica y Parinacota','Camarones']
	comunas['15202'] = ['15','Arica y Parinacota','General Lagos']
	comunas['15201'] = ['15','Arica y Parinacota','Putre']
	#I REGION
	comunas['1107'] = ['1','Tarapacá','Alto Hospicio']
	comunas['1402'] = ['1','Tarapacá','Camiña']
	comunas['1403'] = ['1','Tarapacá','Colchane']
	comunas['1404'] = ['1','Tarapacá','Huara']
	comunas['1101'] = ['1','Tarapacá','Iquique']
	comunas['1405'] = ['1','Tarapacá','Pica']
	comunas['1401'] = ['1','Tarapacá','Pozo Almonte']
	#II REGION
	comunas['2101'] = ['2','Antofagasta','Antofagasta']
	comunas['2201'] = ['2','Antofagasta','Calama']
	comunas['2302'] = ['2','Antofagasta','María Elena']
	comunas['2102'] = ['2','Antofagasta','Mejillones']
	comunas['2202'] = ['2','Antofagasta','Ollagüe']
	comunas['2203'] = ['2','Antofagasta','San Pedro de Atacama']
	comunas['2103'] = ['2','Antofagasta','Sierra Gorda']
	comunas['2104'] = ['2','Antofagasta','Taltal']
	comunas['2301'] = ['2','Antofagasta','Tocopilla']

	#III REGION
	comunas['3302'] = ['3','Atacama','Alto Del Carmen']
	comunas['3102'] = ['3','Atacama','Caldera']
	comunas['3201'] = ['3','Atacama','Chañaral']
	comunas['3101'] = ['3','Atacama','Copiapó']
	comunas['3202'] = ['3','Atacama','Diego de Almagro']
	comunas['3303'] = ['3','Atacama','Freirina']
	comunas['3304'] = ['3','Atacama','Huasco']
	comunas['3103'] = ['3','Atacama','Tierra Amarilla']
	comunas['3301'] = ['3','Atacama','Vallena']

	#IV REGION
	comunas['4103'] = ['4','Coquimbo','Andacollo']
	comunas['4202'] = ['4','Coquimbo','Canela']
	comunas['4302'] = ['4','Coquimbo','Combarbalá']
	comunas['4102'] = ['4','Coquimbo','Coquimbo']
	comunas['4201'] = ['4','Coquimbo','Illapel']
	comunas['4104'] = ['4','Coquimbo','La Higuera']
	comunas['4101'] = ['4','Coquimbo','La Serena']
	comunas['4203'] = ['4','Coquimbo','Los Vilos']
	comunas['4303'] = ['4','Coquimbo','Monte Patria']
	comunas['4301'] = ['4','Coquimbo','Ovalle']
	comunas['4105'] = ['4','Coquimbo','Paihuano']
	comunas['4304'] = ['4','Coquimbo','Punitaqui']
	comunas['4305'] = ['4','Coquimbo','Río Hurtado']
	comunas['4204'] = ['4','Coquimbo','Salamanca']
	comunas['4106'] = ['4','Coquimbo','Vicuña']

	#V REGION
	comunas['5602'] = ['5','Valparaiso','Algarrobo']
	comunas['5402'] = ['5','Valparaiso','Cabildo']
	comunas['5502'] = ['5','Valparaiso','Calera']
	comunas['5302'] = ['5','Valparaiso','Calle Larga']
	comunas['5603'] = ['5','Valparaiso','Cartagena']
	comunas['5102'] = ['5','Valparaiso','Casablanca']
	comunas['5702'] = ['5','Valparaiso','Catemu']
	comunas['5103'] = ['5','Valparaiso','Concón']
	comunas['5604'] = ['5','Valparaiso','El Quisco']
	comunas['5605'] = ['5','Valparaiso','El Tabo']
	comunas['5503'] = ['5','Valparaiso','Hijuelas']
	comunas['5201'] = ['5','Valparaiso','Isla de Pascua']
	comunas['5104'] = ['5','Valparaiso','Juan Fernández']
	comunas['5504'] = ['5','Valparaiso','La Cruz']
	comunas['5401'] = ['5','Valparaiso','La Ligua']
	comunas['5802'] = ['5','Valparaiso','Limache']
	comunas['5703'] = ['5','Valparaiso','Llaillay']
	comunas['5301'] = ['5','Valparaiso','Los Andes']
	comunas['5506'] = ['5','Valparaiso','Nogales']
	comunas['5803'] = ['5','Valparaiso','Olmué']
	comunas['5704'] = ['5','Valparaiso','Panquehue']
	comunas['5403'] = ['5','Valparaiso','Papudo']
	comunas['5404'] = ['5','Valparaiso','Petorca']
	comunas['5105'] = ['5','Valparaiso','Puchuncaví']
	comunas['5705'] = ['5','Valparaiso','Putaendo']
	comunas['5501'] = ['5','Valparaiso','Quillota']
	comunas['5801'] = ['5','Valparaiso','Quilpué']
	comunas['5107'] = ['5','Valparaiso','Quintero']
	comunas['5303'] = ['5','Valparaiso','Rinconada']
	comunas['5601'] = ['5','Valparaiso','San Antonio']
	comunas['5304'] = ['5','Valparaiso','San Esteban']
	comunas['5701'] = ['5','Valparaiso','San Felipe']
	comunas['5706'] = ['5','Valparaiso','Santa María']
	comunas['5606'] = ['5','Valparaiso','Santo Domingo']
	comunas['5101'] = ['5','Valparaiso','Valparaíso']
	comunas['5804'] = ['5','Valparaiso','Villa Alemana']
	comunas['5109'] = ['5','Valparaiso','Viña del Mar']
	comunas['5405'] = ['5','Valparaiso','Zapallar']

	#XIII REGION
	comunas['13502'] = ['13','Metropolitana','Alhué']
	comunas['13402'] = ['13','Metropolitana','Buin']
	comunas['13403'] = ['13','Metropolitana','Calera de Tango']
	comunas['13102'] = ['13','Metropolitana','Cerrillos']
	comunas['13103'] = ['13','Metropolitana','Cerro Navia']
	comunas['13301'] = ['13','Metropolitana','Colina']
	comunas['13104'] = ['13','Metropolitana','Conchalí']
	comunas['13503'] = ['13','Metropolitana','Curacaví']
	comunas['13105'] = ['13','Metropolitana','El Bosque']
	comunas['13602'] = ['13','Metropolitana','El Monte']
	comunas['13106'] = ['13','Metropolitana','Estación Central']
	comunas['13107'] = ['13','Metropolitana','Huechuraba']
	comunas['13108'] = ['13','Metropolitana','Independencia']
	comunas['13603'] = ['13','Metropolitana','Isla de Maipo']
	comunas['13109'] = ['13','Metropolitana','La Cisterna']
	comunas['13110'] = ['13','Metropolitana','La Florida']
	comunas['13111'] = ['13','Metropolitana','La Granja']
	comunas['13112'] = ['13','Metropolitana','La Pintana']
	comunas['13113'] = ['13','Metropolitana','La Reina']
	comunas['13302'] = ['13','Metropolitana','Lampa']
	comunas['13114'] = ['13','Metropolitana','Las Condes']
	comunas['13115'] = ['13','Metropolitana','Lo Barnechea']
	comunas['13116'] = ['13','Metropolitana','Lo Espejo']
	comunas['13117'] = ['13','Metropolitana','Lo Prado']
	comunas['13118'] = ['13','Metropolitana','Macul']
	comunas['13119'] = ['13','Metropolitana','Maipú']
	comunas['13504'] = ['13','Metropolitana','María Pinto']
	comunas['13501'] = ['13','Metropolitana','Melipilla']
	comunas['13120'] = ['13','Metropolitana','Ñuñoa']
	comunas['13604'] = ['13','Metropolitana','Padre Hurtado']
	comunas['13404'] = ['13','Metropolitana','Paine']
	comunas['13121'] = ['13','Metropolitana','Pedro Aguirre Cerda']
	comunas['13605'] = ['13','Metropolitana','Peñaflor']
	comunas['13122'] = ['13','Metropolitana','Peñalolén']
	comunas['13202'] = ['13','Metropolitana','Pirque']
	comunas['13123'] = ['13','Metropolitana','Providencia']
	comunas['13124'] = ['13','Metropolitana','Pudahuel']
	comunas['13201'] = ['13','Metropolitana','Puente Alto']
	comunas['13125'] = ['13','Metropolitana','Quilicura']
	comunas['13126'] = ['13','Metropolitana','Quinta Normal']
	comunas['13127'] = ['13','Metropolitana','Recoleta']
	comunas['13128'] = ['13','Metropolitana','Renca']
	comunas['13401'] = ['13','Metropolitana','San Bernardo']
	comunas['13129'] = ['13','Metropolitana','San Joaquín']
	comunas['13203'] = ['13','Metropolitana','San José de Maipo']
	comunas['13130'] = ['13','Metropolitana','San Miguel']
	comunas['13505'] = ['13','Metropolitana','San Pedro']
	comunas['13131'] = ['13','Metropolitana','San Ramón']
	comunas['13101'] = ['13','Metropolitana','Santiago']
	comunas['13601'] = ['13','Metropolitana','Talagante']
	comunas['13303'] = ['13','Metropolitana','Til Til']
	comunas['13132'] = ['13','Metropolitana','Vitacura']

	#VI REGION
	comunas['6302'] = ['6','Libertador General Bernardo OHiggins','Chépica']
	comunas['6303'] = ['6','Libertador General Bernardo OHiggins','Chimbarongo']
	comunas['6102'] = ['6','Libertador General Bernardo OHiggins','Codegua']
	comunas['6103'] = ['6','Libertador General Bernardo OHiggins','Coinco']
	comunas['6104'] = ['6','Libertador General Bernardo OHiggins','Coltauco']
	comunas['6105'] = ['6','Libertador General Bernardo OHiggins','Doñihue']
	comunas['6106'] = ['6','Libertador General Bernardo OHiggins','Graneros']
	comunas['6202'] = ['6','Libertador General Bernardo OHiggins','La Estrella']
	comunas['6107'] = ['6','Libertador General Bernardo OHiggins','Las Cabras']
	comunas['6203'] = ['6','Libertador General Bernardo OHiggins','Litueche']
	comunas['6304'] = ['6','Libertador General Bernardo OHiggins','Lolol']
	comunas['6108'] = ['6','Libertador General Bernardo OHiggins','Machalí']
	comunas['6109'] = ['6','Libertador General Bernardo OHiggins','Malloa']
	comunas['6204'] = ['6','Libertador General Bernardo OHiggins','Marchihue']
	comunas['6110'] = ['6','Libertador General Bernardo OHiggins','Mostazal']
	comunas['6305'] = ['6','Libertador General Bernardo OHiggins','Nancagua']
	comunas['6205'] = ['6','Libertador General Bernardo OHiggins','Navidad']
	comunas['6111'] = ['6','Libertador General Bernardo OHiggins','Olivar']
	comunas['6306'] = ['6','Libertador General Bernardo OHiggins','Palmilla']
	comunas['6206'] = ['6','Libertador General Bernardo OHiggins','Paredones']
	comunas['6307'] = ['6','Libertador General Bernardo OHiggins','Peralillo']
	comunas['6112'] = ['6','Libertador General Bernardo OHiggins','Peumo']
	comunas['6113'] = ['6','Libertador General Bernardo OHiggins','Pichidegua']
	comunas['6201'] = ['6','Libertador General Bernardo OHiggins','Pichilemu']
	comunas['6308'] = ['6','Libertador General Bernardo OHiggins','Placilla']
	comunas['6309'] = ['6','Libertador General Bernardo OHiggins','Pumanque']
	comunas['6114'] = ['6','Libertador General Bernardo OHiggins','Quinta de Tilcoco']
	comunas['6101'] = ['6','Libertador General Bernardo OHiggins','Rancagua']
	comunas['6115'] = ['6','Libertador General Bernardo OHiggins','Rengo']
	comunas['6116'] = ['6','Libertador General Bernardo OHiggins','Requínoa']
	comunas['6301'] = ['6','Libertador General Bernardo OHiggins','San Fernando']
	comunas['6117'] = ['6','Libertador General Bernardo OHiggins','San Vicente']
	comunas['6310'] = ['6','Libertador General Bernardo OHiggins','Santa Cruz']

	#VII REGION
	comunas['7201'] = ['7','Maule','Cauquenes']
	comunas['7202'] = ['7','Maule','Chanco']
	comunas['7402'] = ['7','Maule','Colbún']
	comunas['7102'] = ['7','Maule','Constitución']
	comunas['7103'] = ['7','Maule','Curepto']
	comunas['7301'] = ['7','Maule','Curicó']
	comunas['7104'] = ['7','Maule','Empedrado']
	comunas['7302'] = ['7','Maule','Hualañé']
	comunas['7303'] = ['7','Maule','Licantén']
	comunas['7401'] = ['7','Maule','Linares']
	comunas['7403'] = ['7','Maule','Longaví']
	comunas['7105'] = ['7','Maule','Maule']
	comunas['7304'] = ['7','Maule','Molina']
	comunas['7404'] = ['7','Maule','Parral']
	comunas['7106'] = ['7','Maule','Pelarco']
	comunas['7203'] = ['7','Maule','Pelluhue']
	comunas['7107'] = ['7','Maule','Pencahue']
	comunas['7305'] = ['7','Maule','Rauco']
	comunas['7405'] = ['7','Maule','Retiro']
	comunas['7108'] = ['7','Maule','Río Claro']
	comunas['7306'] = ['7','Maule','Romeral']
	comunas['7307'] = ['7','Maule','Sagrada Familia']
	comunas['7109'] = ['7','Maule','San Clemente']
	comunas['7406'] = ['7','Maule','San Javier']
	comunas['7110'] = ['7','Maule','San Rafael']
	comunas['7101'] = ['7','Maule','Talca']
	comunas['7308'] = ['7','Maule','Teno']
	comunas['7309'] = ['7','Maule','Vichuquén']
	comunas['7407'] = ['7','Maule','Villa Alegre']
	comunas['7408'] = ['7','Maule','Yerbas Buenas']

	#VIII REGION
	comunas['8314'] = ['8','Biobío','Alto Biobío']
	comunas['8302'] = ['8','Biobío','Antuco']
	comunas['8202'] = ['8','Biobío','Arauco']
	comunas['8402'] = ['8','Biobío','Bulnes']
	comunas['8303'] = ['8','Biobío','Cabrero']
	comunas['8203'] = ['8','Biobío','Cañete']
	comunas['8103'] = ['8','Biobío','Chiguayante']
	comunas['8401'] = ['8','Biobío','Chillán']
	comunas['8406'] = ['8','Biobío','Chillán Viejo']
	comunas['8403'] = ['8','Biobío','Cobquecura']
	comunas['8404'] = ['8','Biobío','Coelemu']
	comunas['8405'] = ['8','Biobío','Coihueco']
	comunas['8101'] = ['8','Biobío','Concepción']
	comunas['8204'] = ['8','Biobío','Contulmo']
	comunas['8102'] = ['8','Biobío','Coronel']
	comunas['8205'] = ['8','Biobío','Curanilahue']
	comunas['8407'] = ['8','Biobío','El Carmen']
	comunas['8104'] = ['8','Biobío','Florida']
	comunas['8112'] = ['8','Biobío','Hualpén']
	comunas['8105'] = ['8','Biobío','Hualqui']
	comunas['8304'] = ['8','Biobío','Laja']
	comunas['8201'] = ['8','Biobío','Lebu']
	comunas['8206'] = ['8','Biobío','Los Álamos']
	comunas['8301'] = ['8','Biobío','Los Ángeles']
	comunas['8106'] = ['8','Biobío','Lota']
	comunas['8305'] = ['8','Biobío','Mulchén']
	comunas['8306'] = ['8','Biobío','Nacimiento']
	comunas['8307'] = ['8','Biobío','Negrete']
	comunas['8408'] = ['8','Biobío','Ninhue']
	comunas['8409'] = ['8','Biobío','Ñiquén']
	comunas['8410'] = ['8','Biobío','Pemuco']
	comunas['8107'] = ['8','Biobío','Penco']
	comunas['8411'] = ['8','Biobío','Pinto']
	comunas['8412'] = ['8','Biobío','Portezuelo']
	comunas['8308'] = ['8','Biobío','Quilaco']
	comunas['8309'] = ['8','Biobío','Quilleco']
	comunas['8413'] = ['8','Biobío','Quillón']
	comunas['8414'] = ['8','Biobío','Quirihue']
	comunas['8415'] = ['8','Biobío','Ranquil']
	comunas['8416'] = ['8','Biobío','San Carlos']
	comunas['8417'] = ['8','Biobío','San Fabián']
	comunas['8418'] = ['8','Biobío','San Ignacio']
	comunas['8419'] = ['8','Biobío','San Nicolás']
	comunas['8108'] = ['8','Biobío','San Pedro de la Paz']
	comunas['8310'] = ['8','Biobío','San Rosendo']
	comunas['8311'] = ['8','Biobío','Santa Bárbara']
	comunas['8109'] = ['8','Biobío','Santa Juana']
	comunas['8110'] = ['8','Biobío','Talcahuano']
	comunas['8207'] = ['8','Biobío','Tirúa']
	comunas['8111'] = ['8','Biobío','Tomé']
	comunas['8420'] = ['8','Biobío','Treguaco']
	comunas['8312'] = ['8','Biobío','Tucapel']
	comunas['8313'] = ['8','Biobío','Yumbel']
	comunas['8421'] = ['8','Biobío','Yungay']

	#IX REGION
	comunas['9201'] = ['9','Araucanía','Angol']
	comunas['9102'] = ['9','Araucanía','Carahue']
	comunas['9121'] = ['9','Araucanía','Cholchol']
	comunas['9202'] = ['9','Araucanía','Collipulli']
	comunas['9103'] = ['9','Araucanía','Cunco']
	comunas['9203'] = ['9','Araucanía','Curacautín']
	comunas['9104'] = ['9','Araucanía','Curarrehue']
	comunas['9204'] = ['9','Araucanía','Ercilla']
	comunas['9105'] = ['9','Araucanía','Freire']
	comunas['9106'] = ['9','Araucanía','Galvarino']
	comunas['9107'] = ['9','Araucanía','Gorbea']
	comunas['9108'] = ['9','Araucanía','Lautaro']
	comunas['9109'] = ['9','Araucanía','Loncoche']
	comunas['9205'] = ['9','Araucanía','Lonquimay']
	comunas['9206'] = ['9','Araucanía','Los Sauces']
	comunas['9207'] = ['9','Araucanía','Lumaco']
	comunas['9110'] = ['9','Araucanía','Melipeuco']
	comunas['9111'] = ['9','Araucanía','Nueva Imperial']
	comunas['9112'] = ['9','Araucanía','Padre Las Casas']
	comunas['9113'] = ['9','Araucanía','Perquenco']
	comunas['9114'] = ['9','Araucanía','Pitrufquén']
	comunas['9115'] = ['9','Araucanía','Pucón']
	comunas['9208'] = ['9','Araucanía','Purén']
	comunas['9209'] = ['9','Araucanía','Renaico']
	comunas['9116'] = ['9','Araucanía','Saavedra']
	comunas['9101'] = ['9','Araucanía','Temuco']
	comunas['9117'] = ['9','Araucanía','Teodoro Schmidt']
	comunas['9118'] = ['9','Araucanía','Toltén']
	comunas['9210'] = ['9','Araucanía','Traiguén']
	comunas['9211'] = ['9','Araucanía','Victoria']
	comunas['9119'] = ['9','Araucanía','Vilcún']
	comunas['9120'] = ['9','Araucanía','Villarrica']

	#X REGION
	comunas['14102'] = ['14','Los Ríos','Corral']
	comunas['14202'] = ['14','Los Ríos','Futrono']
	comunas['14201'] = ['14','Los Ríos','La Unión']
	comunas['14203'] = ['14','Los Ríos','Lago Ranco']
	comunas['14103'] = ['14','Los Ríos','Lanco']
	comunas['14104'] = ['14','Los Ríos','Los Lagos']
	comunas['14105'] = ['14','Los Ríos','Mafil']
	comunas['14106'] = ['14','Los Ríos','Mariquina']
	comunas['14107'] = ['14','Los Ríos','Paillaco']
	comunas['14108'] = ['14','Los Ríos','Panguipulli']
	comunas['14204'] = ['14','Los Ríos','Río Bueno']
	comunas['14101'] = ['14','Los Ríos','Valdivia']
	#XII REGION

	comunas['10202'] = ['10','Los Lagos','Ancud']
	comunas['10102'] = ['10','Los Lagos','Calbuco']
	comunas['10201'] = ['10','Los Lagos','Castro']
	comunas['10401'] = ['10','Los Lagos','Chaitén']
	comunas['10203'] = ['10','Los Lagos','Chonchi']
	comunas['10103'] = ['10','Los Lagos','Cochamó']
	comunas['10204'] = ['10','Los Lagos','Curaco de Vélez']
	comunas['10205'] = ['10','Los Lagos','Dalcahue']
	comunas['10104'] = ['10','Los Lagos','Fresia']
	comunas['10105'] = ['10','Los Lagos','Frutillar']
	comunas['10402'] = ['10','Los Lagos','Futaleufú']
	comunas['10403'] = ['10','Los Lagos','Hualaihue']
	comunas['10107'] = ['10','Los Lagos','Llanquihue']
	comunas['10106'] = ['10','Los Lagos','Los Muermos']
	comunas['10108'] = ['10','Los Lagos','Maullín']
	comunas['10301'] = ['10','Los Lagos','Osorno']
	comunas['10404'] = ['10','Los Lagos','Palena']
	comunas['10101'] = ['10','Los Lagos','Puerto Montt']
	comunas['10302'] = ['10','Los Lagos','Puerto Octay']
	comunas['10109'] = ['10','Los Lagos','Puerto Varas']
	comunas['10206'] = ['10','Los Lagos','Puqueldón']
	comunas['10303'] = ['10','Los Lagos','Purranque']
	comunas['10304'] = ['10','Los Lagos','Puyehue']
	comunas['10207'] = ['10','Los Lagos','Queilén']
	comunas['10208'] = ['10','Los Lagos','Quellón']
	comunas['10209'] = ['10','Los Lagos','Quemchi']
	comunas['10210'] = ['10','Los Lagos','Quinchao']
	comunas['10305'] = ['10','Los Lagos','Río Negro']
	comunas['10306'] = ['10','Los Lagos','San Juan de la Costa']
	comunas['10307'] = ['10','Los Lagos','San Pablo']

	#XI REGION
	comunas['11201'] = ['11','Aysén del General Carlos Ibáñez del Campo','Aisén']
	comunas['11401'] = ['11','Aysén del General Carlos Ibáñez del Campo','Chile Chico']
	comunas['11202'] = ['11','Aysén del General Carlos Ibáñez del Campo','Cisnes']
	comunas['11301'] = ['11','Aysén del General Carlos Ibáñez del Campo','Cochrane']
	comunas['11101'] = ['11','Aysén del General Carlos Ibáñez del Campo','Coyhaique']
	comunas['11203'] = ['11','Aysén del General Carlos Ibáñez del Campo','Guaitecas']
	comunas['11102'] = ['11','Aysén del General Carlos Ibáñez del Campo','Lago Verde']
	comunas['11302'] = ['11','Aysén del General Carlos Ibáñez del Campo','OHiggins']
	comunas['11402'] = ['11','Aysén del General Carlos Ibáñez del Campo','Río Ibáñez']
	comunas['11303'] = ['11','Aysén del General Carlos Ibáñez del Campo','Tortel']

	comunas['12202'] = ['12','Magallanes y de la Antártica Chilena','Antártica']
	comunas['12201'] = ['12','Magallanes y de la Antártica Chilena','CABO DE HORNOS']
	comunas['12102'] = ['12','Magallanes y de la Antártica Chilena','Laguna Blanca']
	comunas['12401'] = ['12','Magallanes y de la Antártica Chilena','Natales']
	comunas['12301'] = ['12','Magallanes y de la Antártica Chilena','Porvenir']
	comunas['12302'] = ['12','Magallanes y de la Antártica Chilena','Primavera']
	comunas['12101'] = ['12','Magallanes y de la Antártica Chilena','Punta Arenas']
	comunas['12103'] = ['12','Magallanes y de la Antártica Chilena','Río Verde']
	comunas['12104'] = ['12','Magallanes y de la Antártica Chilena','San Gregorio']
	comunas['12303'] = ['12','Magallanes y de la Antártica Chilena','Timaukel']
	comunas['12402'] = ['12','Magallanes y de la Antártica Chilena','Torres del Paine']


	#1 : TELEFONIA_MOVIL
	#2 : TELEFONIA FIJA
	#3 : INTERNET
	#4 : TELEVISION
	def start_requests (self):
		for comuna in self.comunas:
			url = 'http://cpt.subtel.cl/ComPlan/regionComuna.do?method=next&codRegionSelected='+self.comunas[comuna][0]+'&codComunaSelected='+comuna
			yield Request (url = url
						  ,callback = self.parse_main
						  ,dont_filter = True
						  ,meta = {'codigo_comuna':comuna}
						  )
	
	def parse_main(self, response):
		#MOVIL
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=agregarServicio&codServ=4"
					 , callback = self.parse_dummy
					 , meta = {'codigo_comuna':response.meta['codigo_comuna']}
					 , dont_filter = True

					 )

		
	def parse_dummy (self, response):
		yield Request( url="http://cpt.subtel.cl/ComPlan/seleccionServicio.do?method=next"
					 , callback=self.parse_tv
					 , dont_filter = True
					 , meta = {'codigo_comuna':response.meta['codigo_comuna']}

					 )

	def parse_tv(self,response):
		for row in response.xpath("//table[@class='tablesorter']//tbody//tr"):
			loader = SubtelLoader(response=response, selector = row)
			loader.add_xpath(  "nombre_empresa"			, "./td[2]//text()")
			loader.add_xpath(  "nombre_plan"			, "./td[3]//text()")
			loader.add_xpath(  "valor_plan"				, "./td[4]//text()")
			loader.add_xpath(  "cantidad_canales"		, "./td[5]//text()")

			loader.add_value(  "servicio"				, "TV")
			loader.add_value(  "region"					, self.comunas[response.meta['codigo_comuna']][1])
			loader.add_value(  "comuna"					, self.comunas[response.meta['codigo_comuna']][2])
			loader.add_value(  "hora_extraccion"  		, time.strftime("%H:%M:%S"))
			loader.add_value(  "fecha_extraccion" 		, time.strftime("%d/%m/%Y"))
			loader.add_value(  "fuente"           		, "www.subtel.cl")
			yield(loader.load_item())
	