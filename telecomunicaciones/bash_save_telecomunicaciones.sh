#!/bin/bash

PATH=$PATH:/usr/local/bin
export PATH

cd /home/spot1/telecom_subtel/telecomunicaciones/spiders

scrapy runspider subtel_fija_cl.py
scrapy runspider subtel_fija_internet_cl.py
scrapy runspider subtel_internet_cl.py
scrapy runspider subtel_movil_cl.py
scrapy runspider subtel_trio_cl.py
scrapy runspider subtel_tv_cl.py
scrapy runspider subtel_tv_fija_cl.py



#Se crea archivos de errores
echo 'Archivos csv generados por script del mismo nombre sin la palabra output_ y extension .csv' >> errores.csv

for entry in $(ls output_*)
do
   if [ -s ${entry} ];then
       echo archivo correcto: ${entry}
       #Enviamos los datos a S3
       aws s3 cp ${entry} s3://spot-backup/spot-telecomunicaciones/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d/
       aws s3 mv ${entry} s3://spot-telecomunicaciones/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d/

   else
       echo archivo vacío : ${entry}
       echo  ${entry} >> errores.csv
   fi
done

#Se sube archivo general  output.csv
#aws s3 mv output.csv s3://spot-telecomunicaciones/descargasScrapy/outputfiles/`date +%Y%m%d_%H`_d.csv

#Se sube archivo errores
aws s3 cp errores.csv s3://spot-backup/spot-telecomunicaciones/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv
aws s3 mv errores.csv s3://spot-telecomunicaciones/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv

#Se eliminan archivos .pyc y .csv
cd /home/spot1/telecomunicaciones
find . -name \*.pyc -delete
find . -name \*.csv -delete

cd /home/spot1/cronlog
aws s3 cp bash_`date +%Y%m%d_%H`_d.log  s3://spot-backup/spot-telecomunicaciones/descargasScrapy/bash_run/bash_`date +%Y%m%d_%H`_d.log
aws s3 mv bash_`date +%Y%m%d_%H`_d.log  s3://spot-telecomunicaciones/descargasScrapy/bash_run/bash_`date +%Y%m%d_%H`_d.log
find . -name \*.log -delete



